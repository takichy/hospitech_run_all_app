# Hospitech_Run_All_APP_At_The_Same_Time

## Description

ce script c'est pour lancer toute l'application Hospitech côté web en même temps, il suffit que de lancer le fichier .sh en local

## Préparation avant le lancement de script

Pour lancer le script d'abord vous devriez avoir docker installer et lancer en local.

Aprés le lancement de script vous pouvez taper l'url:

[Link text Here](http://localhost:3000) Pour voir la première version de l'application web qui contient plus de fonctionnalités.

[Link text Here](http://localhost:7777/logon) Pour voir la 2éme version qu'on est en train de refaire, avec moins de fonctionnalités que la première.
